#include "CSV/CSVHandler.h"
#include "NeuralNetworks/Perceptron.h"
#include "NeuralNetworks/AdalineGD.h"

std::vector<int> initY(const std::vector<std::vector<std::string>>& iris) {
    std::vector<int> y;
    y.reserve(100);
    for (int i = 0; i < 100; ++i) {
        y.push_back(iris.at(i).at(4) == "Iris-setosa" ? -1 : 1);
    }
    return y;
}
std::vector<std::vector<double>> initX(const std::vector<std::vector<std::string>>& iris, const int& quantity) {
    std::vector<std::vector<double>> X;
    for (int i = 0; i < 100; ++i) {
        std::vector<double> row;
        row.reserve(2);
        for (int j = 0; j < quantity; ++j) {
            row.push_back(std::stod(iris.at(i).at(j)));
        }
        X.push_back(row);
    }
    return X;
}
void quality(const std::vector<int>& y, const std::vector<int>& predicts) {
    if (y.size() != predicts.size()) {
        return;
    }
    int correct_predictions = 0;
    for (int i = 0; i < y.size(); ++i) {
        if (y[i] == predicts[i]) {
            correct_predictions++;
        }
    }
    double accuracy = static_cast<double>(correct_predictions) / (double)y.size() * 100;
    std::cout << "Accuracy Perceptron: " << accuracy << "%" << std::endl;
}

void runPerceptron(const std::vector<int>& y, const std::vector<std::vector<double>>& X) {
    Perceptron perceptron(0.01, 100);
    perceptron.fit(X, y);

    std::vector<int> q;
    q.reserve(X.size());
    for (const auto& item : X) {
        q.push_back(perceptron.predict(item));
    }
    quality(y, q);
}
void runAdalineGD(const std::vector<int>& y, const std::vector<std::vector<double>>& X) {
    AdalineGD adalineGd(0.0001, 100);
    adalineGd.fit(X, y);

    std::vector<int> q;
    q.reserve(X.size());
    for (const auto& item : X) {
        q.push_back(adalineGd.predict(item));
    }
    quality(y, q);
}

int main() {
    CSVHandler csvHandler;
    auto iris = new std::vector<std::vector<std::string>>(csvHandler.readCSV("Data/iris.csv"));

    std::vector<int> y = initY(*iris);
    std::vector<std::vector<double>> X = initX(*iris, 2);

    runPerceptron(y, X);
    runAdalineGD(y, X);

    delete iris;

    return 0;
}


