//
// Created by Rostyslav Lugovyy on 04.06.23.
//

#ifndef CSVREADER_H
#define CSVREADER_H

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

class CSVHandler {
private:
    char delimiter;
public:
    explicit CSVHandler(char delimiter = ',');
    [[nodiscard]] std::vector<std::vector<std::string>> readCSV(const std::string&) const;
};

#endif //CSVREADER_H
