//
// Created by Rostyslav Lugovyy on 04.06.23.
//

#include "CSVHandler.h"

#include <utility>

CSVHandler::CSVHandler(char delimiter)
        : delimiter(delimiter) {}

[[maybe_unused]] std::vector<std::vector<std::string>> CSVHandler::readCSV(const std::string& filename) const {
    std::vector<std::vector<std::string>> data;

    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cout << "Failed to open file: " << filename << std::endl;
        return data;
    }

    std::string line;
    while (std::getline(file, line)) {
        std::vector<std::string> row;
        std::stringstream ss(line);
        std::string cell;

        while (std::getline(ss, cell, delimiter)) {
            row.push_back(cell);
        }
        data.push_back(row);
    }
    file.close();

    return data;
}
