cmake_minimum_required(VERSION 3.21)
project(ML1)

set(CMAKE_CXX_STANDARD 20)
add_executable(ML1 main.cpp CSV/CSVHandler.cpp CSV/CSVHandler.h NeuralNetworks/Perceptron.cpp NeuralNetworks/Perceptron.h NeuralNetworks/AdalineGD.cpp NeuralNetworks/AdalineGD.h)
