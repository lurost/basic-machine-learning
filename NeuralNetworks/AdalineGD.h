//
// Created by Rostyslav Lugovyy on 07.07.23.
//

#ifndef ML1_ADALINEGD_H
#define ML1_ADALINEGD_H

#include <iostream>
#include <vector>

class AdalineGD {
private:
    double eta;
    int n_iter;
    std::vector<double> weight;
    std::vector<double> errors;
    std::vector<double> costs;
public:
    explicit AdalineGD(double eta = 0.01, int n_iter = 100);

    void fit(const std::vector<std::vector<double>>& X, const std::vector<int>& y);
    double net_input(const std::vector<double>&);
    std::vector<double> activation(const std::vector<std::vector<double>>&);
    int predict(const std::vector<double>& X);
};


#endif //ML1_ADALINEGD_H
