//
// Created by Rostyslav Lugovyy on 07.07.23.
//

#include "AdalineGD.h"

AdalineGD::AdalineGD(double eta, int n_iter) :
        eta(eta), n_iter(n_iter) {}

void AdalineGD::fit(const std::vector<std::vector<double>>& X, const std::vector<int>& y) {
    this->weight.resize(1 + X[0].size(), 0.0);
    this->errors.resize(y.size(), 0.0);
    this->costs.clear();

    for (int i = 0; i < this->n_iter; i++) {
        std::vector<double> output = activation(X);
        for (int j = 0; j < y.size(); j++) {
            errors[j] = (double)y[j] - output[j];
        }
        for (int j = 0; j < X[0].size(); j++) {
            double update = 0.0;
            for (int k = 0; k < X.size(); k++) {
                update += this->eta * errors[k] * X[k][j];
            }
            this->weight[j + 1] += update;
        }

        for (int j = 0; j < X.size(); j++) {
            this->weight[0] += this->eta * errors[j];
        }

        double cost = 0.0;
        for (const auto& error : errors) {
            cost += std::pow(error, 2);
        }
        cost /= 2.0;
        this->costs.push_back(cost);
    }
}

double AdalineGD::net_input(const std::vector<double>& X) {
    /// Рассчитать чистый вход
    double net_input = this->weight[0];
    for (int i {0}; i < X.size(); i++) {
        net_input += this->weight[i + 1] * X[i];
    }
    return net_input;
}
std::vector<double> AdalineGD::activation(const std::vector<std::vector<double>> & X) {
    std::vector<double> output;
    output.reserve(X.size());
    for (const auto& item : X) {
        output.push_back(net_input(item));
    }
    return output;
}

int AdalineGD::predict(const std::vector<double>& X) {
    /// Вернуть метку класса после единичного скачка
    return (net_input(X) >= 0.0) ? 1 : -1;
}