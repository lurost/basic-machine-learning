//
// Created by Rostyslav Lugovyy on 04.06.23.
//

#include "Perceptron.h"

Perceptron::Perceptron(double eta, int n_iter) :
        eta(eta), n_iter(n_iter) {}

void Perceptron::fit(const std::vector<std::vector<double>>& X, const std::vector<int>& y) {
    this->weight.resize(1 + X[0].size(), 0.0);
    this->errors.clear();

    for (int i {0}; i < this->n_iter; i++) {
        int error = 0;
        for (int j {0}; j < X.size(); j++) {
            double update = this->eta * (y[j] - predict(X[j]));
            for (int k {0}; k < X[j].size(); k++) {
                this->weight[k + 1] += update * X[j][k];
            }
            this->weight[0] += update;
            error += (update != 0.0) ? 1 : 0;
        }
        this->errors.push_back(error);
    }
}

double Perceptron::net_input(const std::vector<double>& X) {
    /// Рассчитать чистый вход
    double net_input = this->weight[0];
    for (int i {0}; i < X.size(); i++) {
        net_input += this->weight[i + 1] * X[i];
    }
    return net_input;
}

int Perceptron::predict(const std::vector<double>& X) {
    /// Вернуть метку класса после единичного скачка
    return (net_input(X) >= 0.0) ? 1 : -1;
}

