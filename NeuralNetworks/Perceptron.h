//
// Created by Rostyslav Lugovyy on 04.06.23.
//

#ifndef ML1_PERCEPTRON_H
#define ML1_PERCEPTRON_H

#include <iostream>
#include <vector>

class Perceptron {
private:
    double eta;
    int n_iter;
    std::vector<double> weight;
    std::vector<int> errors;
public:
    explicit Perceptron(double eta = 0.01, int n_iter = 100);

    void fit(const std::vector<std::vector<double>>& X, const std::vector<int>& y);
    double net_input(const std::vector<double>& X);
    int predict(const std::vector<double>& X);
};

#endif //ML1_PERCEPTRON_H
